# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=k9s
_pkgname=github.com/derailed/k9s
pkgver=0.24.13
pkgrel=0
_commit=1f63270 # git rev-parse --short HEAD
_date=2021-07-02T07:15:49UTC # date -u -d @$(date +%s) +%FT%T%Z
pkgdesc="Kubernetes TUI"
url="https://k9scli.io"
# riscv64 FTBS
arch="all !x86 !armhf !mips64 !riscv64" # tests fail
license="Apache-2.0"
makedepends="go"
options="net chmod-clean"
source="$pkgname-$pkgver.tar.gz::https://github.com/derailed/k9s/archive/v$pkgver.tar.gz"

export GOPATH="$srcdir/go"
export GOCACHE="$srcdir/go-build"
export GOTMPDIR="$srcdir"

build() {
	local ldflags="-w -s -X $_pkgname/cmd.version=$pkgver -X $_pkgname/cmd.commit=$_commit -X $_pkgname/cmd.date=$_date"
	go build -ldflags "$ldflags" -a -tags netgo -o execs/$pkgname main.go
}

check() {
	go test ./...
}

package() {
	install -Dm755 execs/$pkgname "$pkgdir"/usr/bin/$pkgname
}

sha512sums="
736ceedab7d7f725f3933079b50d6a5c7f129fc8080d99ee79ec9be33c0799d7f975347d18b9c7af1e700ef7de39e3dc57ae97da927bb88db8b79102ab9ff7de  k9s-0.24.13.tar.gz
"
